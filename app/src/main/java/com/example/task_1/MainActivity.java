package com.example.task_1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView txtHello = (TextView) findViewById(R.id.txtHello);
        txtHello.setText("");

        Button btnPrintText = (Button) findViewById(R.id.btnPrintText);
        Button btnClearText = (Button) findViewById(R.id.btnClearText);

        btnPrintText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtHello.setText("Hello, World!");
            }
          }
        );

        btnClearText.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                txtHello.setText("");
            }
        });
    }
}
